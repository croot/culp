//import plugins from 'gulp-load-plugins';
const gulp = require('gulp')
const plugins = require('gulp-load-plugins')
const runSeq = require('run-sequence')
const $ = plugins({
    pattern : ['*'],
    scope : ['devDependencies'],
    rename: {
        'merge-stream': 'merge',
        'vinyl-buffer': 'buffer',
        'gulp-file-include': 'include',
        'gulp-html-beautify': 'htmlbeautify',
        'gulp-iconfont-css': 'iconfontCss'
    }
});

/* PATH CONFIG */
const pathSrc = 'src'
const pathResource = 'static'
const pathDst = 'build'
const path = {
    src: {
        'root':   [pathSrc],
        'html':   [pathSrc,'html'],
        'script': [pathSrc,pathResource,'script'],
        'style':  [pathSrc,pathResource,'style'],
        'image':  [pathSrc,pathResource,'image'],
        'font':   [pathSrc,pathResource,'font']
    },
    dst: {
        'root':   [pathDst],
        'html':   [pathDst,'html'],
        'script': [pathDst,pathResource,'script'],
        'style':  [pathDst,pathResource,'style'],
        'image':  [pathDst,pathResource,'image'],
        'font':   [pathDst,pathResource,'font']
    }
};
/* CUSTOM CONFIG - COMMON */
//지원 브라우저 (참고: https://github.com/browserslist/browserslist)
const BROWSER_SUPPORT = ["> 1%", "IE 10"]
// Charactor set
const ENCODING = 'UTF-8';
// LOCAL SERVER CONFIG
const SERVER_PORT = 8000;
// HTML Reporter
const REPORT_FILE_NAME = 'htmlhint-reporter.html'

/* CUSTOM CONFIG - SCRIPT */
const JS_FILE_NAME = 'app.min.js'
const JSLIB_DIR_NAME = 'lib'
const JSLIB_FILE_NAME = 'vendor.min.js'

/* CUSTOM CONFIG - SPRITE */
//적용대상 폴더이름
const SPRITE_IMG_DIR_NAME = 'sprites'
//산출이미지 이름
const SPRITE_IMG_NAME = 'sprite'
//산출스타일 이름
const SPRITE_CSS_NAME = '_sprite.scss' // or 'sprite.css'
//아이콘 클래스명 접두사 ( ex: sp-iconName )
const SPRITE_ICON_PREFIX = 'sp'
//Retina 사용 유무
const USE_RETINA = false
//Retina 이미지명 필터
const RETINA_FILTER = '@x2'

/* CUSTOM CONFIG - ICONICFONT */
ICONIC_FONT_USE = true
//웹폰트 폴더 이름
ICONIC_FONT_DIR_NAME = 'webfont'
//웹폰트명
ICONIC_FONT_NAME = 'iconic'
//웹폰트 스타일 이름
ICONIC_FONT_CSS_NAME = '_icons.scss'

/* CUSTOM CONFIG - COPY FILE */
//추가적으로 복사가 필요한 파일 목록
const FILELIST = ['**/*.{mp3,mp4,pdf,doc,docx,xls,xlsx,ppt,pptx}']

/************************************/

for( let key in path.src) {
    path.src[key] = path.src[key].join('/')
}
for( let key in path.dst) {
    path.dst[key] = path.dst[key].join('/')
}

/* TASK DEFINE */
/* CLEAN */
gulp.task('clean', ()=>{
    return gulp.src([
                    pathDst,
                    path.src.style+'/'+SPRITE_CSS_NAME,
                    path.src.style+'/'+WEBFONT_CSS_NAME,
                    path.src.font+'/'+WEBFONT_NAME+'.*',

                ])
                .pipe($.clean({force: true}))
});

/* STYLE - SASS */
gulp.task('sass', ()=>{
    return gulp.src(['**/*.scss','!**/_*.scss'],{cwd:path.src.style})
                .pipe($.newer(path.dst.style))
                .pipe($.sourcemaps.init({loadMaps: true}))
                .pipe($.sass({outputStyle: 'compressed', sourceMap: true}).on('error', $.sass.logError))
                .pipe($.autoprefixer({
                    browsers: BROWSER_SUPPORT,
                    cascade: false
                }))
                .pipe($.sourcemaps.write('./'))
                .pipe($.rename({suffix:'.min'}))
                .pipe(gulp.dest(path.dst.style));
});

/* STYLE - DEV */
gulp.task('style:dev', ['sass'], function(){
    return gulp.src('**/*.css',{cwd:path.src.style})
                .pipe($.newer(path.dst.style))
                .pipe($.autoprefixer({
                    browsers: BROWSER_SUPPORT,
                    cascade: false
                }))
                .pipe($.cssbeautify())
                .pipe($.rename({suffix:'.min'}))
                .pipe($.charset({to:ENCODING}))
                .pipe(gulp.dest(path.dst.style))
});

/* STYLE - PROD */
gulp.task('style:prod', ['sass'], ()=>{
    return gulp.src('**/*.css',{cwd:path.src.style})
                .pipe($.autoprefixer({
                    browsers: BROWSER_SUPPORT,
                    cascade: false
                }))
                .pipe($.cssmin())
                .pipe($.rename({suffix:'.min'}))
                .pipe($.charset({to:ENCODING}))
                .pipe(gulp.dest(path.dst.style));
});

/* HTML - DEV */
gulp.task('html:dev', ()=>{
    return gulp.src('**/*.html',{cwd:path.src.html})
                .pipe($.newer(path.dst.html))
                .pipe($.include({
                    prefix: '<!-- ',
                    suffix: ' -->',
                    basepath: '@file'
                }))
                .pipe($.charset({to:ENCODING,quiet:false}))
                .pipe(gulp.dest(path.dst.html));
});

/* HTML - PROD */
gulp.task('html:prod', ()=>{
    return gulp.src('**/*.html',{cwd:path.src.html})
                .pipe($.include({
                    prefix: '<!-- ',
                    suffix: ' -->',
                    basepath: '@file'
                }))
                .pipe($.htmlbeautify())
                .pipe($.htmlhint('.htmlhintrc'))
                .pipe($.htmlhint.reporter(require('gulp-htmlhint-html-reporter'), {
                    filename: REPORT_FILE_NAME,
                    createMissingFolders : false  
                  }))
                .pipe($.charset({to:ENCODING}))
                .pipe(gulp.dest(path.dst.html));
});

/* SCRIPT - DEV */
gulp.task('script:dev', ()=>{
    return $.merge(
                //Normal
                gulp.src(['**/*.js','!'+JSLIB_DIR_NAME+'/**/*.js','!**/*.min.js'],{cwd:path.src.script})
                    .pipe($.newer(path.dst.script))
                    .pipe($.sourcemaps.init({loadMaps: true}))
                    .pipe($.babel({
                        presets: ['env']
                    }))
                    .pipe($.jsbeautifier())
                    .pipe($.sourcemaps.write('./')),
                //Libraries
                gulp.src([JSLIB_DIR_NAME+'/**/*.js'],{cwd:path.src.script})
                    .pipe($.newer(path.dst.script))
                    .pipe($.concat(JSLIB_FILE_NAME))
                    .pipe($.uglify()),
                //Minimize
                gulp.src(['**/*.min.js','!'+JSLIB_DIR_NAME+'/**/*.js'],{cwd:path.src.script})
                    .pipe($.newer(path.dst.script))
            )
            .pipe($.charset({to:ENCODING}))
            .pipe(gulp.dest(path.dst.script));
});

/* SCRIPT - PROD */
gulp.task('script:prod', ()=>{
    return $.merge(
                //Normal
                gulp.src(['**/*.js','!'+JSLIB_DIR_NAME+'/**/*.js'],{cwd:path.src.script})
                    .pipe($.babel({
                        presets: ['env']
                    }))
                    .pipe($.sourcemaps.init({loadMaps: true}))
                    .pipe($.concat(JS_FILE_NAME))
                    .pipe($.uglify({mangle: false}))
                    .pipe($.sourcemaps.write('./')),
                //Libraries
                gulp.src([JSLIB_DIR_NAME+'/**/*.js'],{cwd:path.src.script})
                    .pipe($.concat(JSLIB_FILE_NAME))
                    .pipe($.uglify({mangle: false}))
            )
            .pipe($.charset({to:ENCODING}))
            .pipe(gulp.dest(path.dst.script));
});

/* IMAGE - DEV */
gulp.task('image:dev', ()=>{
    //copy
    gulp.src(['**/*.{jpg,jpeg,gif,png,svg}','!'+SPRITE_IMG_DIR_NAME+'/*.png'],{cwd:path.src.image})
        .pipe($.newer(path.dst.image))
        .pipe($.imagemin())
        .pipe(gulp.dest(path.dst.image));
});

/* IMAGE - PROD */
gulp.task('image:prod', ['sprites'], ()=>{
    //copy
    gulp.src(['**/*.{jpg,jpeg,gif,png,svg}','!'+SPRITE_IMG_DIR_NAME+'/*.png'],{cwd:path.src.image})
        .pipe($.imagemin())
        .pipe(gulp.dest(path.dst.image));
});

/* IMAGE - SPRITES */
gulp.task('sprites', ()=>{
    const imgDirName = [path.src.image,SPRITE_IMG_DIR_NAME].join('/')
    const imgPath = path.dst.image.replace('/'+path.dst.root+'/','/')
    let option = {
        imgPath,
        imgName: SPRITE_IMG_NAME + '.png',
        cssPath: path.src.style,
        cssName: SPRITE_CSS_NAME,
        padding: 6,
        cssVarMap: function (sprite) {
            sprite.name = SPRITE_ICON_PREFIX + '-' + sprite.name;
        }
    }

    /* Retina Config */
    if(USE_RETINA) {
        option.retinaSrcFilter = [imgDirName, '*@x2.png'].join('/')
        option.retinaImgPath = imgPath
        option.retinaImgName = SPRITE_IMG_NAME + RETINA_FILTER + '.png'
    }

    /* Create Task */
    let spriteData = gulp.src([imgDirName, '*.png'].join('/')).pipe($.spritesmith(option));
    return $.merge(
            //image stream
            spriteData.img
                .pipe($.buffer())
                .pipe($.imagemin())
                .pipe(gulp.dest(path.dst.image)),
            //scss stream
            spriteData.css
                .pipe(gulp.dest(path.src.style))
        );
});

/* FONT - FONTICON */
gulp.task('fontIcon', ()=>{
    if(ICONIC_FONT_USE) {
        const targetPath = ['..', ((/\/([a-zA-Z0-9._]+)(?:\?.*)?$/).test(path.src.style) && RegExp.$1), WEBFONT_CSS_NAME].join('/')
        const fontPath = ['..', ((/\/([a-zA-Z0-9._]+)(?:\?.*)?$/).test(path.dst.font) && RegExp.$1), ''].join('/')

        return gulp.src(ICONIC_FONT_DIR_NAME+'/*.svg',{cwd:path.src.font})
            .pipe($.iconfontCss({
                fontName: ICONIC_FONT_NAME,
                targetPath,
                fontPath
            }))
            .pipe($.iconfont({
                fontName: ICONIC_FONT_NAME,
                prependUnicode: true,
                formats: ['eot', 'woff2', 'woff'],
                timestamp: (Date.now()/1000)
            }))
            .pipe(gulp.dest(path.src.font));
    }
});

/* FONT */
gulp.task('font', ()=>{
    gulp.src('**/*.{eot,woff,woff2,ttf,otf}',{cwd:path.src.font})
        .pipe($.newer(path.src.font))
        .pipe(gulp.dest(path.dst.font));
});

/* FILE */
gulp.task('file', ()=>{
    gulp.src(FILELIST,{ cwd: path.src.root })
        .pipe($.newer(path.dst.root))
        .pipe(gulp.dest(path.dst.root));
});

/* SERVER */
gulp.task('connect', ()=>{
    connect.server({
        root: path.dst.root,
        port: SERVER_PORT,
        middleware: (connect, opt)=>{
            connect.mime.charsets.lookup = (types=>ENCODING)
            return [];
        }
    });
});

gulp.task('watch', ()=>{
    gulp.watch('**/*.{html,inc}', {cwd:path.src.html}, ['html:dev']);
    gulp.watch('*.scss', {cwd:path.src.style}, ['sass:dev']);
    gulp.watch('*.css', {cwd:path.src.style}, ['css:dev']);
    gulp.watch('**/*.js', {cwd:path.src.script}, ['script:dev']);
    gulp.watch(['**/*.{jpg,jpeg,gif,png,svg}','!'+SPRITE_IMG_DIR_NAME+'/*.png'], {cwd:path.src.image}, ['image:dev']);
    gulp.watch([SPRITE_IMG_DIR_NAME+'/*.png'], {cwd:path.src.image}, ['sprites','sass']);
    gulp.watch('**/*.{eot,woff,woff2,ttf,otf}', {cwd:path.src.font}, ['font']);
    if(ICONIC_FONT_USE)
        gulp.watch(WEBFONT_DIR_NAME+'/*.svg', {cwd:path.src.font}, ['fontIcon','font']);
    gulp.watch(FILELIST, {cwd:path.src.root}, ['file']);
});

//개발 용
gulp.task('dev', ['clean'], (callback)=>{
    runSeq(
        ['sprites','fontIcon'],
        ['html:dev','style:dev','script:dev'],
        ['font','file'],
        ['connect','watch'],
        callback);
});

//산출물 용
gulp.task('prod', ['clean'], (callback)=>{
    runSeq(
        ['sprites','fontIcon'],
        ['html:prod', 'style:prod', 'script:prod'],
        ['font','file'],
        callback);
});