<div class="path">
    <div class="inner">
        <ul class="path-list">
            <li><a href="javascript:">기업소개</a>
                <ul>
                    <li><a href="javascript:">기업소개</a>
                        <ul>
                            <li><a href="/html/pc/about-us/vision.html">기업소개</a>
                                <ul class="last">
                                    <li><a href="/html/pc/about-us/intro-vision.html">비전 및 경영전략</a></li>
                                    <li><a href="/html/pc/about-us/intro-greeting.html">CEO 인사말</a></li>
                                    <li><a href="/html/pc/about-us/intro-info.html">기업정보</a></li>
                                </ul>
                            </li>
                            <li><a href="/html/pc/about-us/lgway-management01.html">LG Way</a>
                                <ul class="last">
                                    <li><a href="/html/pc/about-us/lgway-management01.html">LG Way와 정도 경영</a></li>
                                    <li><a href="/html/pc/about-us/lgway-ethic02.html">윤리규범</a></li>
                                    <li><a href="/html/pc/about-us/lgway-program03.html">정도경영 프로그램</a></li>
                                    <li><a href="/html/pc/about-us/lgway-sinmungo04.html">사이버 신문고</a></li>
                                </ul>
                            </li>
                            <li><a href="/html/pc/about-us/rnd-introductionofstudy.html">R&amp;D</a>
                                <ul class="last">
                                    <li><a href="/html/pc/about-us/rnd-introductionofstudy.html">연구 소개</a></li>
                                    <li><a href="/html/pc/about-us/rnd-fieldofstudy01.html">연구 분야</a></li>
                                    <li><a href="/html/pc/about-us/rd-result.html">연구 결과</a></li>
                                </ul>
                            </li>
                            <li><a href="/html/pc/about-us/design-center.html">디자인</a>
                                <ul class="last">
                                    <li><a href="/html/pc/about-us/design-center.html">디자인센터</a></li>
                                    <li><a href="/html/pc/about-us/design-ability.html">디자인 역량</a></li>
                                    <li><a href="/html/pc/about-us/design-result.html">디자인 성과 (어워즈)</a></li>
                                </ul>
                            </li>
                            <li><a href="/html/pc/about-us/csm-system.html">지속가능경영</a>
                                <ul class="last">
                                    <li><a href="/html/pc/about-us/csm-system.html">지속가능경영</a></li>
                                    <li><a href="/html/pc/about-us/csm-green.html">그린경영</a></li>
                                    <li><a href="/html/pc/about-us/csm-quality.html">품질경영</a></li>
                                    <li><a href="/html/pc/about-us/csm-growth.html">동반성장</a></li>
                                    <li><a href="/html/pc/about-us/csm-contribution01.html">사회공헌</a></li>
                                </ul>
                            </li>
                            <li><a href="/html/pc/about-us/pr-notice.html">홍보센터</a>
                                <ul class="last">
                                    <li><a href="/html/pc/about-us/pr-notice.html">공지사항</a></li>
                                    <li><a href="/html/pc/about-us/pr-news.html">뉴스</a></li>
                                    <li><a href="/html/pc/about-us/pr-ad-video.html">광고</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="javascript:">제품</a>
                        <ul>
                            <li><a href="javascript:">건축장식자재 !!Z:IN!!</a>
                                <ul class="last">
                                    <li><a href="javascript:">창호</a></li>
                                    <li><a href="javascript:">유리</a></li>
                                    <li><a href="javascript:">바닥제</a></li>
                                    <li><a href="javascript:">벽지</a></li>
                                    <li><a href="javascript:">인조대리석 &amp; 이스톤</a></li>
                                    <li><a href="javascript:">PF 단열재</a></li>
                                    <li><a href="javascript:">합성목재</a></li>
                                    <li><a href="javascript:">데코 필름</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:">고기능소재</a>
                                <ul class="last">
                                    <li><a href="javascript:">가전필름</a></li>
                                    <li><a href="javascript:">Sign &amp; Graphic</a></li>
                                    <li><a href="javascript:">진공단열재</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:">자동차소재부품</a>
                                <ul class="last">
                                    <li><a href="javascript:">자동차 원단</a></li>
                                    <li><a href="javascript:">자동차 경량화부품</a></li>
                                    <li><a href="javascript:">자동차 일반부품</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="/html/pc/investment/council-organization.html">투자정보</a>
                        <ul>
                            <li><a href="/html/pc/investment/council-organization.html">이사회</a>
                                <ul class="last">
                                    <li><a href="/html/pc/investment/council-organization.html">이사회 구성</a></li>
                                    <li><a href="/html/pc/investment/council-report01.html">이사회 운영 현황</a></li>
                                    <li><a href="/html/pc/investment/council-report01.html">산하위원회 운영 현황</a></li>
                                    <li><a href="/html/pc/investment/council-articles-of-association.html">회사정관</a></li>
                                </ul>
                            </li>
                            <li><a href="/html/pc/investment/ir.html">IR 활동</a>
                                <ul class="last">
                                    <li><a href="/html/pc/investment/ir.html">목록</a></li>
                                    <li><a href="javascript:">상세보기</a></li>
                                </ul>
                            </li>
                            <li><a href="/html/pc/investment/stockholder01.html">주식정보</a>
                                <ul class="last">
                                    <li><a href="/html/pc/investment/stockholder01.html">주가정보</a></li>
                                    <li><a href="/html/pc/investment/stockholder02.html">주가 / 주식현황</a></li>
                                </ul>
                            </li>
                            <li><a href="/html/pc/investment/financial-highlight01.html">재무정보(IR자료)</a>
                                <ul class="last">
                                    <li><a href="/html/pc/investment/financial-highlight01.html">재무 하이라이트</a></li>
                                    <li><a href="/html/pc/investment/financial-rate.html">재무 비율</a></li>
                                    <li><a href="/html/pc/investment/financial-inspection.html">감사보고서</a></li>
                                    <li><a href="/html/pc/investment/financial-business.html">영업보고서</a></li>
                                    <li><a href="/html/pc/investment/financial-credit-rating.html">신용평가등급</a></li>
                                </ul>
                            </li>
                            <li><a href="/html/pc/investment/financial-result-data.html">실적자료</a>
                                <ul class="last">
                                    <li><a href="/html/pc/investment/financial-result-data.html">실적자료</a></li>
                                </ul>
                            </li>
                            <li><a href="/html/pc/investment/financial-disclosure.html">공시자료</a>
                                <ul class="last">
                                    <li><a href="/html/pc/investment/financial-disclosure.html">공시정보</a></li>
                                    <li class="current"><a href="/html/pc/investment/financial-rules.html">공시정보 관리규정</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="/html/pc/careers/employment-talent.html">인재채용</a>
                        <ul>
                            <li><a href="/html/pc/careers/employment-talent.html">인재상</a>
                                <ul class="last">
                                    <li><a href="/html/pc/careers/employment-talent.html">인재상</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:">인사제도</a>
                                <ul class="last">
                                    <li><a href="/html/pc/careers/employment-manage01.html">인사원칙</a></li>
                                    <li><a href="/html/pc/careers/employment-manage02.html">인재육성</a></li>
                                    <li><a href="/html/pc/careers/employment-manage03.html">보상제도</a></li>
                                    <li><a href="/html/pc/careers/employment-manage04.html">복리후생</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:">지원가이드</a>
                                <ul class="last">
                                    <li><a href="/html/pc/careers/employment-process01.html">채용프로세스</a></li>
                                    <li><a href="/html/pc/careers/job-introduce.html">직무소개</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:">하우시스 人의 이야기</a>
                                <ul class="last">
                                    <li><a href="/html/pc/careers/hausyspeople-story.html">목록</a></li>
                                    <li><a href="/html/pc/careers/hausyspeople-story-view.html">상세</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:">채용공고</a>
                                <ul class="last">
                                    <li><a href="/html/pc/careers/recruit-office.html">사무직 채용공고</a></li>
                                    <li><a href="/html/pc/careers/recruit-technical-sales.html">기능직 및 직영 영업직</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>