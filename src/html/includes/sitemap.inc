
<aside id="sitemap" class="menu-sitemap">
    <h2 class="title">전체 메뉴 보기</h2>
    <fieldset class="search">
        <input type="text">
        <a href="javascript:"></a>
    </fieldset>
    <div class="box-sitemap">
        <div class="part">
            <h3>기업소개</h3>
            <ul>
                <li><a href="javascript:">기업소개</a>
                    <ul>
                        <li><a href="/html/about-us/intro-vision.html">비전 및 경영전략</a></li>
                        <li><a href="/html/about-us/intro-greeting.html">CEO 인사말</a></li>
                        <li><a href="/html/about-us/intro-info.html">기업정보</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">LG Way</a>
                    <!--
                    <ul>
                        <li><a href="javascript:;">LG Way와 정도경영</a></li>
                        <li><a href="javascript:;">윤리규범</a></li>
                        <li><a href="javascript:;">정도경영 프로그램</a></li>
                        <li><a href="javascript:;">사이버 신문고</a></li>
                    </ul>
                    -->
                </li>
                <li><a href="javascript:">R&amp;D</a>
                    <ul>
                        <li><a href="/html/about-us/rnd-introductionofstudy.html">연구 소개</a></li>
                        <li><a href="/html/about-us/rnd-fieldofstudy01.html">연구 분야</a></li>
                        <li><a href="/html/about-us/rd-result.html">연구 결과</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">디자인</a>
                    <ul>
                        <li><a href="/html/about-us/design-center.html">디자인센터</a></li>
                        <li><a href="/html/about-us/design-ability.html">디자인 역량</a></li>
                        <li><a href="/html/about-us/design-result.html">디자인 성과(어워즈)</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">지속가능경영</a>
                    <ul>
                        <li><a href="/html/about-us/csm-system.html">지속가능경영</a></li>
                        <li><a href="javascript:">그린경영</a></li>
                        <li><a href="javascript:">품질경영</a></li>
                        <li><a href="javascript:">동반성장</a></li>
                        <li><a href="javascript:">사회공헌</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">홍보센터</a>
                    <ul>
                        <li><a href="/html/about-us/pr-notice.html">공지사항</a></li>
                        <li><a href="/html/about-us/pr-news.html">뉴스</a></li>
                        <li><a href="/html/about-us/pr-ad-video.html">광고</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="part">
            <h3>제품</h3>
            <ul>
                <li><a href="javascript:">건축장식자재 <span class="ziin"></span></a>
                    <ul>
                        <li><a href="javascript:">창호</a></li>
                        <li><a href="javascript:">유리</a></li>
                        <li><a href="javascript:">커튼월</a></li>
                        <li><a href="javascript:">벽지</a></li>
                        <li><a href="javascript:">바닥재</a></li>
                        <li><a href="javascript:">우젠</a></li>
                        <li><a href="javascript:">PF단열제</a></li>
                        <li><a href="javascript:">하이막스</a></li>
                        <li><a href="javascript:">비아테라</a></li>
                        <li><a href="javascript:">인테리어 필름</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">고기능소재</a>
                    <ul>
                        <li><a href="javascript:">광고용 시트</a></li>
                        <li><a href="javascript:">가전 표면 소재</a></li>
                        <li><a href="javascript:">데코시트</a></li>
                        <li><a href="javascript:">진공단열재</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">자동차소재부품</a>
                    <ul>
                        <li><a href="javascript:">자동차원단</a></li>
                        <li><a href="javascript:">자동차 경량화부품</a></li>
                        <li><a href="javascript:">자동차 일반부품</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="part">
            <h3>투자정보/IR</h3>
            <ul>
                <li><a href="javascript:">이사회</a>
                    <ul>
                        <li><a href="/html/investment/council-organization.html">이사회 구성</a></li>
                        <li><a href="/html/investment/council-report01.html">이사회 운영 현황</a></li>
                        <li><a href="/html/investment/council-report01.html">산하위원회 운영 현황</a></li>
                        <li><a href="javascript:">회사정관</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">IR활동</a>
                    <ul>
                        <li><a href="javascript:">목록</a></li>
                        <li><a href="javascript:">상세보기</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">주식정보</a>
                    <ul>
                        <li><a href="javascript:">주가정보</a></li>
                        <li><a href="javascript:">주가/주식현황</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">재무정보(IR자료)</a>
                    <ul>
                        <li><a href="javascript:">재무 하이라이트</a></li>
                        <li><a href="javascript:">재무비율</a></li>
                        <li><a href="javascript:">감사보고서</a></li>
                        <li><a href="javascript:">영업보고서</a></li>
                        <li><a href="javascript:">신용평가등급</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">실적자료</a>
                </li>
                <li><a href="javascript:">공시자료</a>
                    <ul>
                        <li><a href="javascript:">공시정보</a></li>
                        <li><a href="javascript:">공시정보 관리규정</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="part">
            <h3>인재채용</h3>
            <ul>
                <li><a href="/html/careers/employment-talent.html">인재상</a>
                </li>
                <li><a href="javascript:">인사제도</a>
                    <ul>
                        <li><a href="/html/careers/employment-manage01.html">인사원칙</a></li>
                        <li><a href="/html/careers/employment-manage02.html">인재육성</a></li>
                        <li><a href="/html/careers/employment-manage03.html">보상제도</a></li>
                        <li><a href="/html/careers/employment-manage04.html">복리후생</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">지원가이드</a>
                    <ul>
                        <li><a href="/html/careers/employment-process01.html">채용프로세스</a></li>
                        <li><a href="/html/careers/job-introduce.html">직무소개</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">하우시스<span style="font-family:'NotoSansKR'; vertical-align:top; font-size:15px">人</span>의 이야기</a>
                    <ul>
                        <li><a href="/html/careers/hausyspeople-story.html">목록</a></li>
                        <li><a href="/html/careers/hausyspeople-story-view.html">상세</a></li>
                    </ul>
                </li>
                <li><a href="javascript:">채용공고</a>
                    <ul>
                        <li><a href="/html/careers/recruit-office.html">사무직 채용공고</a></li>
                        <li><a href="/html/careers/recruit-technical-sales.html">기능직 및 직영 영업직</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="part">
            <h3>고객센터</h3>
            <ul>
                <li class="link-etc"><a href="javascript:">1:1 상담</a>
                    <!--
                    <ul>
                        <li><a href="javascript:;">등록</a></li>
                        <li><a href="javascript:;">완료</a></li>
                    </ul>
                    -->
                </li>
                <li class="link-etc"><a href="javascript:">찾아오시는 길</a>
                </li>
                <li class="link-etc"><a href="javascript:">개인정보처리방침</a>
                </li>
                <li class="link-etc"><a href="javascript:">인재개발센터</a>
                </li>
            </ul>
            <h3 class="link">Link</h3>
            <ul>
                <li class="link-etc"><a href="javascript:">상공간 Library</a></li>
                <li class="link-etc"><a href="javascript:"><span class="ziin"></span></a>
                </li>
                <li class="link-etc"><a href="javascript:"><span class="ziin"></span> mall</a>
                </li>
                <li class="link-etc"><a href="javascript:">Global Network</a>
                </li>
            </ul>
            <h3 class="etc">ETC.</h3>
            <ul>
                <li class="link-etc"><a href="javascript:">정도경영 사이버 신문고</a>
                </li>
                <li class="link-etc"><a href="javascript:">협력회사 상생고</a>
                </li>
                <li class="link-etc"><a href="javascript:">이메일무단수집거부</a>
                </li>
                <li class="link-etc"><a href="javascript:">BIM 라이브러리</a>
                </li>
                <li class="link-etc"><a href="javascript:">LG하우시스 역사관</a>
                </li>
                <li class="link-etc"><a href="javascript:">사이트맵</a>
                </li>
            </ul>
        </div>

        <div class="part-sns">
            <ul>
                <li class="fb"><a href="#"></a></li>
                <li class="insta"><a href="#"></a></li>
                <li class="blog"><a href="#"></a></li>
                <li class="ytb"><a href="#"></a></li>
            </ul>
        </div>

    </div>



    <a href="javascript:" class="close" data-role="button-sitemap-close"><span class="hidden">close</span></a>
</aside>