<footer>

	<div class="top-btn">
		<a href="#header-top">
			<Span>TOP</Span>
		</a>
	</div>

	<div class="menu-bottom">
		<div class="inner">
			<ul>
				<li><a href="javascript:">고객문의</a></li>
				<li><a href="javascript:">개인정보처리방침</a></li>
				<li><a href="javascript:" data-role="popup-button" data-content-id="footerPopup01">이메일무단수집거부</a></li>
				<li><a href="javascript:">사이트맵</a></li>
				<li><a href="javascript:">정도경영사이버신문고</a></li>
				<li><a href="javascript:">협력회사 상생고</a></li>
			</ul>
		</div>
	</div>

	<div class="footer">
		<div class="inner">
			<dl class="company-info">
				<dt>주소 </dt>
				<dd>서울시 영등포구 국제금융로 10 ONE IFC 빌딩 15-19f LG하우시스</dd>
				<dt>사업자등록번호</dt>
				<dd>107-87-18122</dd>
				<dt>대표이사 </dt>
				<dd>오장수</dd>
			</dl>
			<p><small>COPYRIGHT &copy; 2017 LG HAUSYS. ALL RIGHTS RESERVED.</small></p>
			<h2><img src="/resources/images/pc/common/footer-logo.png" alt="LG 하우시스 로고"></h2>
		</div>
	</div>

	<section id="footerPopup01" class="lpopup footer-popup" style="display:none;" >
		<div class="lpopup-head">
			<h1>이메일 무단수집 거부</h1>
		</div>
		<div class="lpopup-body">
			<div class="lpopup-inner">
				<p class="popup-txt">
					본 홈페이지에 게시된 이메일 주소가 전자우편 수집 프로그램이나 그밖의 기술적인 장치를 이용하여 무단으로
					수집되는 것을 거부하며, 이를 위반한 시 정보통신망법에 의해 형사처벌됨을 유념하시기 바랍니다.
				</p>

				<div class="anti-spam">
					불법 스팸 대응센터<a href="http://www.spamcop.or.kr" target="_blank">http://www.spamcop.or.kr</a>
				</div>

			</div>
		</div>
		<div class="btn-type02" style="text-align: center; padding-bottom:30px;">
			<a href="javascript:;" data-role="lpopup-close" >닫기</a>
		</div>
		<a href="javascript:" class="lpopup-close" data-role="lpopup-close">close</a>
	</section>

</footer>


