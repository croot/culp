<div id="header-top" class="topmenu">
	<h1 class="logo">
		<a href="../index.html"><img src="/resources/images/pc/common/logo.png" alt="LG 하우시스 로고"></a>
	</h1>

	<div class="util-sns">
		<ul>
			<li><a href="https://www.facebook.com/LGHausys.Zin" target="blank"><span class="hidden">페이스북</span></a></li>
			<li><a href="https://www.instagram.com/lghausys_zin/" target="blank"><span class="hidden">인스타그램</span></a></li>
			<li><a href="http://blog.naver.com/lghausys_zin" target="blank"><span class="hidden">블로그</span></a></li>
			<li><a href="https://www.youtube.com/user/LGHausysZIN" target="blank"><span class="hidden">유투브</span></a></li>
			<li><a href="#" target="blank"><span class="hidden">지인 시뮬레이션</span></a></li>
			<li><a href="#" target="blank"><span class="hidden">지인 상업공간시공사례</span></a></li>
			<li><a href="#" target="blank"><span class="hidden">지인 전시&amp;매장</span></a></li>
		</ul>
	</div>

	<div class="util-top">
		<a href="javascript:" class="btn-c-center">고객센터</a>
		<a href="javascript:" class="btn-global">Global Network</a>
		<fieldset class="btn-search-wrap">
			<a href="javascript:"><span class="hidden">search</span></a>
			<div class="top-search-box">
				<div class="input-text h-top" >
					<p><input type="text" placeholder="제목+내용을 입력해주세요"> <a class="btn-search"></a></p>
				</div>
			</div>
		</fieldset>
	</div>
</div>

<header>

	<div class="menubar">
		<a href="javascript:" class="btn-sitemap" data-role="button-sitemap-open"><span class="hidden">전체메뉴</span></a>
        <div class="inner">
            <nav class="menu-main">
				<ul>
					<li><a href="/html/pc/about-us/intro-vision.html">기업소개</a>
						<ul>
							<li><p class="head">기업소개</p>
								<p><a href="/html/pc/about-us/intro-vision.html">비전 및 경영전략</a></p>
								<p><a href="/html/pc/about-us/intro-greeting.html">CEO 인사말</a></p>
								<p><a href="/html/pc/about-us/intro-info.html">기업정보</a></p>
							</li>
							<li><p class="head">LG Way</p>
								<p><a href="/html/pc/about-us/lgway-management01.html">LG Way와 정도 경영</a></p>
								<p><a href="/html/pc/about-us/lgway-ethic02.html">윤리규범</a></p>
								<p><a href="/html/pc/about-us/lgway-program03.html">정도경영 프로그램</a></p>
								<p><a href="/html/pc/about-us/lgway-sinmungo04.html">사이버 신문고</a></p>
							</li>
							<li><p class="head">R&D</p>
								<p><a href="/html/pc/about-us/rnd-introductionofstudy.html">연구 소개</a></p>
								<p><a href="/html/pc/about-us/rnd-fieldofstudy01.html">연구 분야</a></p>
								<p><a href="/html/pc/about-us/rd-result.html">연구 결과</a></p>
							</li>
							<li><p class="head">디자인</p>
								<p><a href="/html/pc/about-us/design-center.html">디자인센터</a></p>
								<p><a href="/html/pc/about-us/design-ability.html">디자인 역량</a></p>
								<p><a href="/html/pc/about-us/design-result.html">디자인 성과 (어워즈)</a></p>
							</li>
							<li><p class="head">지속가능경영</p>
								<p><a href="/html/pc/about-us/csm-system.html">지속가능경영</a></p>
								<p><a href="/html/pc/about-us/csm-green.html">그린경영</a></p>
								<p><a href="/html/pc/about-us/csm-quality.html">품질경영</a></p>
								<p><a href="/html/pc/about-us/csm-growth.html">동반성장</a></p>
								<p><a href="/html/pc/about-us/csm-contribution01.html">사회공헌</a></p>
							</li>
							<li><p class="head">홍보센터</p>
								<p><a href="/html/pc/about-us/pr-notice.html">공지사항</a></p>
								<p><a href="/html/pc/about-us/pr-news.html">뉴스</a></p>
								<p><a href="/html/pc/about-us/pr-ad-video.html">광고</a></p>
							</li>
						</ul>
					</li>
					<li><a href="javascript:"> 제품 </a>
						<ul>
							<li class="gnb-space01">
								<div><p class="head prd-logo">건축장식자재 <span></span></p></div>
								<div class="division">
									<p><a href="javascript:">창호</a></p>
									<p><a href="javascript:">유리</a></p>
									<p><a href="javascript:">바닥제</a></p>
								</div>
								<div class="division">
									<p><a href="javascript:">벽지</a></p>
									<p><a href="javascript:">인조대리석 &amp; 이스톤</a></p>
									<p><a href="javascript:">PF 단열재</a></p>
								</div>
								<div class="division">
									<p><a href="javascript:">합성목재</a></p>
									<p><a href="javascript:">데코 필름</a></p>
								</div>
							</li>
							<li>
								<p class="head">고기능소재</p>
								<p><a href="javascript:">가전필름</a></p>
								<p><a href="javascript:">Sign &amp; Graphic</a></p>
								<p><a href="javascript:">진공단열재</a></p>
							</li>
							<li>
								<p class="head">자동차소재부품</p>
								<p><a href="javascript:">자동차 원단</a></p>
								<p><a href="javascript:">자동차 경량화부품</a></p>
								<p><a href="javascript:">자동차 일반부품</a></p>
							</li>
						</ul>
					</li>
					<li><a href="/html/pc/investment/council-organization.html">투자정보</a>
						<ul>
							<li>
								<p class="head">이사회</p>
								<p><a href="/html/pc/investment/council-organization.html">이사회 구성</a></p>
								<p><a href="/html/pc/investment/council-report01.html">이사회 운영 현황</a></p>
								<p><a href="/html/pc/investment/council-report02.html">산하위원회 운영 현황</a></p>
								<p><a href="/html/pc/investment/council-articles-of-association.html">회사정관</a></p>
							</li>
							<li>
								<p class="head">IR 활동</p>
								<p><a href="/html/pc/investment/ir.html">목록</a></p>
								<p><a href="javascript:">상세보기</a></p>
							</li>
							<li>
								<p class="head">주식정보</p>
								<p><a href="/html/pc/investment/stockholder01.html">주가정보</a></p>
								<p><a href="/html/pc/investment/stockholder02.html">주가 / 주식현황</a></p>
							</li>
							<li>
								<p class="head">재무정보(IR자료)</p>
								<p><a href="/html/pc/investment/financial-highlight01.html">재무 하이라이트</a></p>
								<p><a href="/html/pc/investment/financial-rate.html">재무 비율</a></p>
								<p><a href="/html/pc/investment/financial-inspection.html">감사보고서</a></p>
								<p><a href="/html/pc/investment/financial-business.html">영업보고서</a></p>
								<p><a href="/html/pc/investment/financial-credit-rating.html">신용평가등급</a></p>
							</li>
							<li>
								<p class="head">실적자료</p>
								<p><a href="/html/pc/investment/financial-result-data.html">실적자료</a></p>
							</li>
							<li>
								<p class="head">공시자료</p>
								<p><a href="/html/pc/investment/financial-disclosure.html">공시정보</a></p>
								<p><a href="/html/pc/investment/financial-rules.html">공시정보 관리규정</a></p>
							</li>
						</ul>
					</li>
					<li><a href="/html/pc/careers/employment-talent.html">인재채용</a>
						<ul>
							<li>
								<p class="head">인재상</p>
								<p><a href="/html/pc/careers/employment-talent.html">인재상</a></p>
							</li>
							<li>
								<p class="head">인사제도</p>
								<p><a href="/html/pc/careers/employment-manage01.html">인사원칙</a></p>
								<p><a href="/html/pc/careers/employment-manage02.html">인재육성</a></p>
								<p><a href="/html/pc/careers/employment-manage03.html">보상제도</a></p>
								<p><a href="/html/pc/careers/employment-manage04.html">복리후생</a></p>
							</li>
							<li>
								<p class="head">지원가이드</p>
								<p><a href="/html/pc/careers/employment-process01.html">채용프로세스</a></p>
								<p><a href="/html/pc/careers/job-introduce.html">직무소개</a></p>
							</li>
							<li>
								<p class="head">하우시스 <span style="font-family:'NotoSansKR'; vertical-align:top; ">人</span>의 이야기</p>
								<p><a href="/html/pc/careers/hausyspeople-story.html">목록</a></p>
								<p><a href="/html/pc/careers/hausyspeople-story-view.html">상세</a></p>
							</li>
							<li class="gnb-space02">
								<p class="head">채용공고</p>
								<p><a href="/html/pc/careers/recruit-office.html">사무직 채용공고</a></p>
								<p><a href="/html/pc/careers/recruit-technical-sales.html">기능직 및 직영 영업직</a></p>
							</li>
						</ul>
					</li>
					<li><a href="http://www.z-in.co.kr/index.jsp" target="_blank"><img src="/resources/images/pc/common/nav_img.png" alt="z:in"></a></li>
				</ul>
            </nav>
        </div>

	</div>

</header>