/**
 * UI ���� ��ũ��Ʈ
 */

(function($) {
    UI = window.UI;

    UI.commonTab = function() {
        //�� �޴�

        $('[data-role=tab-button]').each(function() {
            var classname = 'current';
            var $this = $(this);
            var contentId = $this.attr('data-content-id');

            //�� �޴� �ʱ�ȭ
            if ($this.hasClass(classname)) {
                $('#' + contentId).show();
                $this.siblings().each(function(index, button) {
                    var _contentId = $(this).attr('data-content-id');
                    $('#' + _contentId).hide();
                });
            }

            //��ư �̺�Ʈ ���ε�
            $this.map(function() {
                if (this.tagName.toUpperCase() != 'A') {
                    return $(this).children('a').get(0);
                } else {
                    return this;
                }
            }).off('click').on('click', function(event) {
                $this.siblings('[data-role=tab-button]').each(function(index, button) {
                    var _$this = $(this).closest('[data-content-id]');
                    var _contentId = _$this.attr('data-content-id');

                    _$this.removeClass(classname);
                    if (document.getElementById(_contentId)) {
                        $('#' + _contentId).hide();
                    }

                });

                //Ŭ�� �� ��ư Ȱ��ȭ
                $this.closest('[data-content-id]').addClass(classname);
                if (document.getElementById(contentId)) {
                    $('#' + contentId).show();
                }

                return false;
            });
        })
    };

    UI.accordion = function() {
        //���ڵ��
        $(".ir-list > li").eq(0).addClass('on');
        $(".location-l-box .ir-list > li").addClass('on');

        $('[data-role=accordion-button]').each(function() {
            var classname = 'on';
            var timing = 400;
            var easing = '';
            var $this = $(this);
            var contentId = $this.attr('data-content-id');

            //���ڵ�� �ʱ�ȭ
            if ($this.hasClass(classname)) {
                $('#' + contentId).show();
            } else {
                $('#' + contentId).hide();
            }

            //��ư �̺�Ʈ ���ε�
            $this.map(function() {
                if (this.tagName.toUpperCase() != 'A') {
                    return $(this).children('a').get(0);
                } else {
                    return this;
                }
            }).off('click').on('click', function(event) {

                var _$this = $this.closest('[data-content-id]');
                //Ŭ�� �� ��ư Ȱ��ȭ
                if (_$this.hasClass(classname)) {
                    _$this.removeClass(classname);
                    if (document.getElementById(contentId)) {
                        var _$content = $('#' + contentId);

                        _$content.stop().slideUp(timing, easing);
                    }
                } else {
                    _$this.addClass(classname);
                    if (document.getElementById(contentId)) {
                        var _$content = $('#' + contentId);

                        _$content.stop().slideDown(timing, easing);
                    }
                }

                return false;
            });
        });
    };

    UI.snsShare = function() {
        var $snsBtn = $(".board-view .view-type01 .sns");
        $snsBtn.on("click", function() {
            if (!$snsBtn.hasClass('on')) {
                $snsBtn.next('.sns-hidden').show();
                $snsBtn.addClass('on')
            } else {
                $snsBtn.next('.sns-hidden').hide();
                $snsBtn.removeClass('on')
            }
        })
    };
    
    UI.tabTypeD = function() {
        var $tabBtn = $('.tab-typeD .tab-right > a');
        var $MenuList = $('.tab-typeD .tab-right > ul');

        var $menuUl = $('.tab-typeD .tab-left');
        
        if ($tabBtn.length <= 0 || $MenuList.length <= 0 || $menuUl.length <= 0) {
            return false;
        }
        
        var $menuPosition = parseInt($('.tab-typeD .tab-left ul .current').offset().left + 2);

        $menuUl.scrollLeft($menuPosition);

        $tabBtn.on('click', function() {
            if (!$tabBtn.hasClass('on')) {
                $MenuList.show();
                $tabBtn.addClass('on');
            } else {
                $MenuList.hide();
                $tabBtn.removeClass('on')
            }
        });
    };

})(jQuery);

jQuery(function($) {

    UI.commonTab();
    UI.accordion();
    UI.snsShare();
    UI.tabTypeD();

});