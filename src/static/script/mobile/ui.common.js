/**
 * UI 공통 스크립트
 */

if (window.UI !== undefined) {
    window.oUI = window.UI;
}
window.UI = {};

(function($) {

    UI.sitemap = function() {
        $('[data-role=button-sitemap-open]').on('click', function() {
            $('#sitemap').addClass('open');
            $('body,html').addClass('dimmed');
     
            return false;
        });

        $('[data-role=button-sitemap-close]').on('click', function() {
            $('#sitemap').removeClass('open');
            $('body,html').removeClass('dimmed');
   
            return false;
        });

        $('[data-role=menu-toggle]').on('click', function() {
            $(this).toggleClass('open');
        });
    };

    UI.selectBox = function() {
        $('.select > a').on('click', function() {
            $('.select ul').stop().slideUp();
            $(this).next().stop().slideToggle(function() {
                $(this).parents('.select').find('> a').toggleClass('open');
            });
        });

        $('.select ul li a').on('click', function() {
            var thisTxt = $(this).text();
            var thisLabel = $(this).parents('.select').find('> a');
            thisLabel.text(thisTxt);
            thisLabel.trigger('click');
            thisLabel.focus();
        });
    };

    UI.topSearch = function() {
        $('[data-role=button-search-open]').on('click', function() {
            $('[data-role=search]').addClass('open');
            return false;
        });
        $('[data-role=button-search-close]').on('click', function() {
            $('[data-role=search]').removeClass('open');
            return false;
        });

    };

 

    UI.lpopUp = function() {

        $("[data-role=popup-button]").on('click', function() {
            var contentId1 = $(this).attr('data-content-id');
            $('#' + contentId1).show();
        });
        $("[data-role=lpopup-close]").click(function() {
            $(this).closest('.lpopup').hide();
        });

    };

    UI.setPath = function() {

        //1뎁스 메뉴 클릭 시
        $(".path-depth1 > a").on("click", function() {
            if ($(this).hasClass('on')) {
                $(this).removeClass('on');
                $(this).next('ul').stop().slideUp(200);
            } else {
                $(this).addClass('on');
                $(this).next('ul').stop().slideDown(200);
            }
        });

        //1뎁스 메뉴 클릭 시
        $(".path-depth1 > ul > li > a").on('click', function() {
            var liClass = $(this.parentNode).attr('class');

            //1뎁스 텍스트 수정
            $('.path-depth1 .depth1').html($(this).text());
            $('.path-depth1 > a').trigger('click');
            $('.path-depth2 .depth2').addClass('on');

            $('.path-depth2 > ul').each(function(i, v) {
                if ($(this).hasClass(liClass)) {
                    $(this).addClass('on');
                    $('.path-depth2 .depth2').html($(this).children().first().text());
                    $(this).stop().slideDown(200).siblings('ul').stop().hide();
                } else {
                    $(this).removeClass('on');
                }
            });

        });

        //2뎁스 클릭 시
        $('.path-depth2 > a').on("click", function() {
            if ($(this).hasClass('on')) {
                $(this).removeClass('on');
                $(this).siblings('ul').stop().slideUp(200);
            } else {
                $(this).addClass('on');
                $(this).siblings('ul.on').stop().slideDown(200);
            }
        })
    };

})(jQuery);

jQuery(function($) {
    UI.sitemap();
    UI.selectBox();
    UI.setPath();
    UI.lpopUp();
    UI.topSearch();


});