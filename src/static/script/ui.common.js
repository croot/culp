/**
 * UI 공통 스크립트
 */

if (window.UI !== undefined) {
    window.oUI = window.UI;
}
window.UI = {};

(function($) {

    UI.sitemap = function() {
        var body = $('body');
        var sitemap = $('#sitemap');
        var controlClass = 'sitemap-opened';

        if (body.length <= 0 || sitemap.length <= 0) {
            return false;
        }

        $('[data-role=button-sitemap-open]').off('click').on('click', function(event) {
            body.addClass(controlClass);
            return false;
        });

        $('[data-role=button-sitemap-close]').off('click').on('click', function(event) {
            body.removeClass(controlClass);
            return false;
        });
    };

    UI.setPath = function() {
        var $path = $('.path-list');
        var current = $path.find('.current');
        var ulList = current.parents('ul');

        if ($path.length <= 0 || ulList.length <= 0 || current.length <= 0) {
            return false;
        }
        //Path 첫번째
        ulList.eq(2).children().each(function(index, li) {
            var _$li = $(li);
            if (_$li.find(current).length) {
                $path.append(_$li.clone());
                $path.children('li').eq(0).children('a').remove();
                $path.children('li').eq(0).prepend(_$li.children('a').clone());
            }
        });

        //Path 두번째
        ulList.eq(1).children().each(function(index, li) {
            var _$li = $(li);
            if (_$li.find(current).length) {
                $path.append(_$li.clone());
                $path.children('li').eq(1).children('a').remove();
                $path.children('li').eq(1).prepend(_$li.children('a').clone());
            }
        });

        //Path 세번째
        ulList.eq(0).children().each(function(index, li) {
            var _$li = $(li);

            if (_$li.hasClass('current')) {
                $path.children('li').eq(2).children('a').remove();
                $path.children('li').eq(2).prepend(_$li.children('a').clone());
            }
        });
    };

    UI.selectBox = function() {
        $('.select > a').on('click', function() {
            $('.select ul').stop().slideUp();
            $(this).next().stop().slideToggle(function() {
                $(this).parents('.select').find('> a').toggleClass('open');
            });
        });

        $('.select ul li a').on('click', function() {
            var thisTxt = $(this).text();
            var thisLabel = $(this).parents('.select').find('> a');
            thisLabel.text(thisTxt);
            thisLabel.data('value', $(this).data('value'));
            thisLabel.trigger('click');
            thisLabel.focus();
        });
    };


    UI.lpopUp = function(){
	    var $contentWrap = $('body').find('.content-wrap');
	    var $popUpNotice = $('.notice-popup');
	    var left = $contentWrap.offset().left;
	    $popUpNotice.css({'left': left , 'display' : 'block'});
	    
	    
	    if($contentWrap.length != 0){
	    	var contentId = "";
		    $("[data-role=popup-button]").on('click',function() {
		    	$('.lpopup').hide();
		    	
			    contentId = $(this).attr('data-content-id');
			    $('#'+contentId).css({'left': left, 'top': ($(document).scrollTop() + 128) + 'px'});
			    $('.creditRating').css({'left': '50%', 'top': '346px'});
			    $('.lpopupLib').css({'left': '100px'});
			    $('.footer-popup').css({'left': '50%', 'bottom': '250px','top':'auto'});
			    
			    $('#' + contentId).show().find('a').eq(0).focus();
			  
			    
			    if($(this).attr('class')=='decoPop'){
			    	var idx=$(this).parent('li').index();
			    	var $tab=$('#tabPopupCon01 .tab-typeA a');
			    	var $decoPop=$('#tabPopupCon01 .deco-film-pop');
			    	$tab.removeClass('current');
			    	$tab.eq(idx).addClass('current');
			    	$decoPop.hide();
			    	$decoPop.eq(idx).show();
			    };
			    
			             
		    });
		    $("[data-role=lpopup-close]").click(function(){
			    $(this).closest('.lpopup').hide();
			    $( "[data-content-id = "+ contentId +"]").focus();
			    contentId="";
		    });
	    }
    };

    UI.topSearch = function() {

        var $topBtnWrap = $(".btn-search-wrap");
        var $topBtn = $(".btn-search-wrap > a");

        $topBtn.on('click', function() {
            if (!$topBtnWrap.hasClass('on')) {
                $topBtnWrap.addClass('on');
            } else {
                $topBtnWrap.removeClass('on');
            }
        })

    };

    UI.wpopup = function(url, pageid, option) {
        window.open(url, pageid, option);
        return false;
    };

    UI.gnb = function(){
        $('.menu-main > ul > li > a').on({
            'mouseenter focus': function(event){
                $(this.parentNode).addClass('on').siblings('li').removeClass('on');
            }
        });
        $('.menu-main').on({
            'mouseleave': function(event) {
                $(this).find('li.on').removeClass('on');
            }
        })
    };
    
    UI.topBtn = function(){
    	$("footer .top-btn").hide();
    	$(window).on('scroll',function(){
    		if($(window).scrollTop()>500){
        		$("footer .top-btn").fadeIn();	
        	}else{
        		$("footer .top-btn").fadeOut();
        	}
    	});

    	$("footer .top-btn a").on('click',function(e){
    		 e.preventDefault();
    		$('html,body').stop().animate({'scrollTop':'0'});
    	}) ;
    }
    
})(jQuery);

jQuery(function($) {

	UI.gnb();
    UI.sitemap();
    UI.selectBox();
    UI.setPath();
    UI.lpopUp();
    UI.topSearch();
    UI.topBtn();
    
    $('.menu-main > ul > li > a').on({
    	'mouseenter focus': function(){
	    	$(this.parentNode).siblings('.on').children('a').css({'color':'#dedede', 'border-bottom-color':'transparent'});
	    },
	    'mouseleave blur': function(){
	    	$(this.parentNode).siblings('.on').children('a').removeAttr('style');
	    }
    });
});