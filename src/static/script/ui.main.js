/**
 * UI 메인 스크립트
 */

(function($) {
    UI = window.UI;

    /**
     * 메인 - 뉴스 슬라이드
     * @param
     * @return
     * */
    
    UI.mainVisual = function(){
    	$('.main-visual-wrap').slick({
			dots: true,
			fade: true,
			cssEase: 'linear',
			autoplay: true,
			autoplaySpeed: 3000
		});
    	
		var idx = $('.main-visual-wrap .main-rolling').length;
		 if(idx == 1){
		    	$('.main-visual .slick-dots').hide();
		    }else{
		    	$('.main-visual .slick-dots').show();
		    };
    }

    
    UI.mainNews = function() {
        var news = $('.visible-news');
        var news_li = $('li', news);

        //넓이 동적 생성.

        var li_width = parseInt(news.width() / 4);
        var li_width_expend = li_width * 2;

        $(news_li).off('mousewheel').on('mousewheel', function(event) {
            var currentIndex = parseInt(news.attr('data-index'));
            var nextIndex = currentIndex - (event.deltaY);
            nextIndex = Math.max(0, Math.min(nextIndex, news_li.length - 1));

            if (isNaN(currentIndex)) {
                return true;
            }

            if (nextIndex <= 0 || nextIndex >= news_li.length - 1) {
                if (currentIndex == nextIndex) {
                    return true;
                }
            }

            $(news_li).eq(nextIndex).children('a').trigger('click');

            return false;
        });

        $('a', news_li).off('click').on('click', function(event) {

            //모션 중복 금지.
            if (news.attr('data-animating') == 'true') {
                return false;
            }

            //현재 뉴스 클릭 허용.
            if ($(this.parentNode).index() == news.attr('data-index')) {
                return true;
            }

            var $this = $(this.parentNode);
            var idx = $this.index();
            var point = 0;
            var timing = 800;
            var easing = 'easeInOutQuint';

            news.attr('data-animating', 'true');

            news_li.stop().removeClass('dimmed');
            if (idx == news_li.length - 1) {
                point = li_width_expend * (news_li.length - 3);
                $this.prev().prev().prevAll().addClass('dimmed');
                $(".scroll-click").delay(200).animate({
                    "right": "60%"
                });
                /* 뒤에서 2번째 클릭 시 위치 처리
                } else if(idx == news_li.length - 2) {
                    point = li_width * (news_li.length - 3)
                 */
            } else {
                point = li_width_expend * idx;
                $this.prevAll().addClass('dimmed');
                $(".scroll-click").delay(200).animate({
                    "right": "34%"
                });
            }

            //기존 애니메이션 동작을 막기 위해 stop 메소드 추가.
            $this.addClass('on').siblings().removeClass('on');
            news_li.filter(function() {
                return $(this).hasClass('on') || $(this).hasClass('dimmed')
            }).delay(10).animate({
                "width": li_width_expend + 'px'
            }, timing, easing);
            news_li.filter(function() {
                return !($(this).hasClass('on') || $(this).hasClass('dimmed'))
            }).delay(10).animate({
                "width": li_width + 'px'
            }, timing, easing);

            $('.list', news).stop().animate({
                "margin-left": -point + "px"
            }, timing, easing, function() {

                news.attr('data-animating', 'false');
                //현재 인덱스 정보 저장
                news.attr('data-index', idx);
            });

            return false;
        });
        
        $(window).on('click',function(event){
            
            var timing = 800;
            var easing = 'easeInOutQuint';
            
            if(news_li.filter('.on').length > 0) {
                news_li.stop().removeClass('dimmed on');
                news.attr('data-animating', 'true');
                $('.list', news).stop().animate({
                    "margin-left": 0
                }, timing, easing, function() {
                    news.attr({'data-index': 0, 'data-animating': 'false'});
                    news.removeAttr('data-animating');
                    news_li.animate({'width':li_width},timing.easing);
                });
            }
        
        });

        //반응형 대응하여 width값과 총 길이를 연산에 대입.
        $('.list', news).css({
            'width': ((news_li.length + 1) * li_width_expend) + 'px',
            // 'height': li_width_expend + 'px',
            'marginLeft': 0
        });

        news_li.outerWidth(li_width).removeClass('on dimmed');

    };

    UI.mainPaging = function() {

        var attr = '';
        var ty = '';
        $('.main-side-btn a').on('click', function(e) {
            e.preventDefault();
            attr = $(this).attr('href');
            ty = $(attr).offset().top;
            $('html,body').stop().animate({
                scrollTop: ty
            });
            return false
        });

        var topY = [],
            top = 0;
        for (var i = 1; i <= 3; i++) {
            topY[i] = $('#pagingPart0' + i).stop().offset().top;
        }

        $(window).on('scroll', function(e) {
            e.preventDefault();
            top = $(window).scrollTop();

            for (var i = 1; i <= 3; i++) {
                topY[i] = $('#pagingPart0' + i).stop().offset().top - 60;
                if (top >= topY[i]) {
                    $('.main-side-btn li').stop().removeClass('on');
                    $('.pagingPart0' + i).stop().parent('li').addClass('on')
                }
            }

        })

    };
    


})(jQuery);

/* Initialize */
jQuery(function($) {

	//UI.mainVisual();
    UI.mainNews();
    UI.mainPaging();
    UI.sitemap();
});