/**
 * UI ���� ��ũ��Ʈ
 */

(function($) {
    UI = window.UI;

    UI.pathAccess = function() {
        var path = $('.path-list');
        var all_li = path.find('li');

        $('a', path).on('focus', function(event) {
            all_li.removeClass('on');
            $(this).parents('li').addClass('on');
        });

        all_li.on('mouseenter', function() {
            all_li.removeClass('on');
            $(this).children('a').focus();
        });

        path.on('mouseleave', function() {
            all_li.removeClass('on');
        });
    };
    /*
    	UI.tabPage = function(){
    	    var $tabTypeC = $(".tab-typeC a");

    		$tabTypeC.on("click",function(){
    			var contentName = $(this).attr('id');

    			$tabTypeC.removeClass('current');
                $(this).addClass('current');

    		    $(".content-wrap .conBox").hide();
                $(this).parents('.content-wrap').next('.content-wrap').find( "." + contentName).show();

            })
        };
    */

    UI.commonTab = function() {
        //�� �޴�

        $('[data-role=tab-button]').each(function() {
            var classname = 'current';
            var $this = $(this);
            var contentId = $this.attr('data-content-id');

            //�� �޴� �ʱ�ȭ
            if ($this.hasClass(classname)) {
                $('#' + contentId).show();
                $this.siblings().each(function(index, button) {
                    var _contentId = $(this).attr('data-content-id');
                    $('#' + _contentId).hide();
                });
            }

            //��ư �̺�Ʈ ���ε�
            $this.map(function() {
                if (this.tagName.toUpperCase() != 'A') {
                    return $(this).children('a').get(0);
                } else {
                    return this;
                }
            }).off('click').on('click', function(event) {
                $this.siblings('[data-role=tab-button]').each(function(index, button) {
                    var _$this = $(this).closest('[data-content-id]');
                    var _contentId = _$this.attr('data-content-id');

                    _$this.removeClass(classname);
                    if (document.getElementById(_contentId)) {
                        $('#' + _contentId).hide();
                    }

                });

                //Ŭ�� �� ��ư Ȱ��ȭ
                $this.closest('[data-content-id]').addClass(classname);
                if (document.getElementById(contentId)) {
                    $('#' + contentId).show();
                }

                return false;
            });
        })
    };

    UI.accordion = function() {
        //���ڵ��
    	
        $(".o-location-box > li").eq(0).children('h3').removeClass('on');
        $(".location .o-location-box > li").children('h3').removeClass('on');
        $(".systemTab .o-location-box > li").children('h3').addClass('on');

        $('[data-role=accordion-button]').each(function() {
            var classname = 'on';
            var timing = 400;
            var easing = '';
            var $this = $(this);
            var contentId = $this.attr('data-content-id');

            //���ڵ�� �ʱ�ȭ
            if (!$this.hasClass(classname)) {
                $('#' + contentId).show();
            } else {
                $('#' + contentId).hide();
            }

            //��ư �̺�Ʈ ���ε�
            $this.map(function() {
                if (this.tagName.toUpperCase() != 'A') {
                    return $(this).children('a').get(0);
                } else {
                    return this;
                }
            }).off('click').on('click', function(event) {

                var _$this = $this.closest('[data-content-id]');
                //Ŭ�� �� ��ư Ȱ��ȭ
                if (!_$this.hasClass(classname)) {
                    _$this.addClass(classname);
                    if (document.getElementById(contentId)) {
                        var _$content = $('#' + contentId);

                        _$content.stop().slideUp(timing, easing);
                    }
                } else {
                    _$this.removeClass(classname);
                    if (document.getElementById(contentId)) {
                        var _$content = $('#' + contentId);

                        _$content.stop().slideDown(timing, easing);
                    }
                }

                return false;
            });
        });
    };
    /*
    	UI.locationInfo = function(){

    		$('.o-location-box > li h3').on('click',function(){
    			var $li = $(this).parent('li');
    			
    			if($li.hasClass('on')){
    				$(this).next('.n-info').stop().slideDown();
    				$li.removeClass('on');
    			}else{
    				$(this).next('.n-info').stop().slideUp();
    				$li.addClass('on');
    			}

    		});

    	};
    */
    UI.boardListType = function() {

        var listType = $(".inc-list-type .list-type a");
        var boardType = $(".inc-list-type ul.bored-type");

        listType.on('click', function() {
            var thisId = $(this).attr('id');

            listType.removeClass('on');
            $(this).addClass('on');

            boardType.removeClass('board-type02 board-type03').addClass(thisId);
            boardType.css({opacity:0}).animate({opacity:1},600,'easeOutCubic');
        });
        boardType.css({opacity:1}); 
        
    };

    UI.adPrint = function() {

        var yearWrap = $(".years-wrap");
        var nextBtn = yearWrap.find(".next");
        var prevBtn = yearWrap.find(".prev");
        var yearUl = yearWrap.find(".years ul");
        var yearLi = yearUl.find("li");
        var yearNum = parseInt(yearLi.length);
        var yearLine = yearWrap.find(".years-line");
        var width = parseInt(yearLi.outerWidth()) + parseInt(yearLi.css('margin-right'));
        var idx = 0;
        var list = $(".board-type05 .list");

        yearLine.css({
            "width": yearNum * width - width
        });

        yearWrap.on('click', 'a.next, a.prev', function() {

            switch ($(this).attr('class')) {
                case 'prev':
                    //idx = (idx<=0) ? 5 : idx - 1;
                    idx--;
                    if (idx <= 0) {
                        idx = 0;
                        prevBtn.addClass('on');
                    } else {
                        yearWrap.find(".click-btn a").removeClass('on');
                    }
                    list.hide();
                    list.eq(idx).show();
                    break;

                case 'next':
                    //idx = ((idx>=5) ? 0 : idx + 1);
                    idx++;
                    if (idx >= yearNum - 1) {
                        idx = yearNum - 1;
                        nextBtn.addClass('on');
                    } else {
                        yearWrap.find(".click-btn a").removeClass('on');
                    }
                    list.hide();
                    list.eq(idx).show();
                    break;
            }
            yearUl.stop().animate({
                'margin-left': -140 * idx
            }, function() {
                yearLi.eq(idx).addClass('on').siblings().removeClass('on');
            });
            yearLine.stop().animate({
                'margin-left': -140 * idx
            });
        });

        yearLi.find('a').on('click', function() {
            idx = parseInt($(this).parent('li').index());
            var leftPos = -width * idx;

            yearUl.stop().animate({
                'margin-left': leftPos
            });
            yearLine.stop().animate({
                'margin-left': leftPos
            });
            yearLi.removeClass('on');
            $(this).parent("li").addClass('on');

            if (idx <= 0) {
                prevBtn.addClass('on');
            } else if (idx >= yearNum - 1) {
                nextBtn.addClass('on');
            } else {
                yearWrap.find(".click-btn a").removeClass('on');
            }

            list.hide();
            list.eq(idx).show();
        })

    };
    UI.paging = function() {
        if ($('body').find('div[class^="design-result-tab0"]').length != 0) {
            var url = window.location.href;
            var url_aux = url.split('=');
            var _max = $('div[class^="design-result-tab0"]').length;

            if (url_aux[1] == undefined) {
                url_aux[0] += '?index';
                url_aux[1] = 1;
            }
            if (url_aux[1] == 1) {
                $('.prev').find('a').attr('href', 'javascript:;');
            } else {
                $('.prev').find('a').attr('href', url_aux[0] + '=' + (url_aux[1] * 1 - 1));
            }
            if (url_aux[1] == _max) {
                $('.next').find('a').attr('href', 'javascript:;');
            } else {
                $('.next').find('a').attr('href', url_aux[0] + '=' + (url_aux[1] * 1 + 1));
            }
            $('.design-result-tab0' + url_aux[1]).css('display', 'block').siblings('div[class^="design-result-tab0"]').css('display', 'none');
            $('.paging li').eq(url_aux[1]).addClass('current').siblings('li').removeClass('current');

        }
    };

    UI.privacyPaging = function() {
        var attr = '';
        var ty = '';
        $('.p-click-box ul li a').on('click', function(e) {
            e.preventDefault();
            attr = $(this).attr('href');
            ty = $(attr).offset().top;
            $('html,body').stop().animate({
                scrollTop: ty
            });
            return false
        });

        $('.welfare-list li a').on('click', function(e) {
            e.preventDefault();
            attr = $(this).attr('href');
            ty = $(attr).offset().top;
            $('html,body').stop().animate({
                scrollTop: ty
            });
            return false
        });
    };
    
    UI.jobPaging = function() {
        var attr = '';
        var ty = '';
        $('.job-info-box a').on('click', function(e) {
            e.preventDefault();
            attr = $(this).attr('href');
            ty = $(attr).offset().top;
            $('.job-info-box a').removeClass('on')
            $(this).addClass('on');
            $('html,body').stop().animate({
                scrollTop: ty
            });
            
            var contentId01= $(attr).attr('data-content-id');
            var contentId02= $('#' + contentId01);
            var contentH3= contentId02.prev('h3');
            if (contentH3.hasClass('on')) {
            	contentH3.removeClass('on');
                contentId02.slideDown();
            } 
            return false
        });

    };

    UI.productWindow = function() {
        var $prdList = $(".prd-intro02 .prd-list li");

        $prdList.hover(function() {
            var dataImageBox = $(this).parent('.prd-list').prev('.prd-img');
var dataImage = $(this).attr("data-image");

if (dataImage) {
    dataImageBox.css("background-image", "url(/resources/images/pc/products/" + dataImage + ")");
            }

        });
    };


})(jQuery);

jQuery(function($) {

    //PATH ���ټ� �۾�.
    UI.pathAccess();
    UI.commonTab();
                UI.accordion();
    //UI.tabPage();
    //UI.locationInfo();
    UI.boardListType();
    UI.adPrint();
    UI.paging();
    UI.privacyPaging();
    UI.jobPaging();UI.productWindow();
   


});